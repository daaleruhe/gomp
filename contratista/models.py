from django.db import models

class Contractor(models.Model):
    primer_nombre = models.CharField(
        max_length=15,
        help_text="Ingresar solo letras")


    segundo_nombre = models.CharField(
        max_length=15, blank=True,
        null=True,
        help_text="Ingresar solo letras")


    primer_apellido = models.CharField(
        max_length=15,
        help_text="Ingresar solo letras")


    segundo_apellido = models.CharField(
        max_length=15,
        help_text="Ingresar solo letras")


    documento_identidad = models.IntegerField(
        unique=True,
        help_text="Ingresar solo números")


    direccion = models.CharField(
        max_length=35)


    telefono = models.IntegerField(
        help_text="Ingresar solo números")


    def nombre_completo(self):
        if self.segundo_nombre:
            nombre = "{0} {1} {2} {3}".format(
                self.primer_apellido,
                self.segundo_apellido,
                self.primer_nombre,
                self.segundo_nombre)
        else:
            nombre = "{0} {1} {2}".format(
                self.primer_apellido,
                self.segundo_apellido,
                self.primer_nombre)

        return nombre


    def __str__(self):
        return self.nombre_completo()
